//
//  DownloadManager.h
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadManager : NSObject

+ (instancetype)manager;

- (void)fetchAllImageObjectsComplition:(void (^)(void))complition;

- (void)downloadFileByUrl:(NSString *)url
               complition:(void (^)(NSString *localPath))complition;

- (void)cancelAll;

@end
