//
//  ImageCell.h
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageObject;

@interface ImageCell : UICollectionViewCell

- (void)configureByImageObject:(ImageObject *)imageObject;

@end
