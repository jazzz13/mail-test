//
//  ImageObject.h
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ImageObject : NSManagedObject

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *localPath;
@property (nonatomic, strong) NSDate *date;

- (void)downloadFile;

@end
