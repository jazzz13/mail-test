//
//  ImageObject.m
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "ImageObject.h"
#import "DownloadManager.h"

@implementation ImageObject

@dynamic localPath;
@dynamic date;
@dynamic url;

- (void)downloadFile
{
    [[DownloadManager manager] downloadFileByUrl:self.url
                                      complition:^(NSString *localPath) {
                                          
                                          self.localPath = localPath;
                                      }];
}

@end
