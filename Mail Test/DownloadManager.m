//
//  DownloadManager.m
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "DownloadManager.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ImageObject.h"

@interface DownloadManager ()

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSMutableDictionary *currentDownloadUrls;

@end

@implementation DownloadManager

+ (instancetype)manager
{
    static DownloadManager *instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [self new];
        
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        _context = appDelegate.managedObjectContext;
        
        [self calculateUrl];
        
        _currentDownloadUrls = [NSMutableDictionary new];
        
        _queue = [NSOperationQueue new];
        
        _queue.maxConcurrentOperationCount = 5;
    }
    
    return self;
}

- (void)calculateUrl
{
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSinceNow:-[NSTimeZone systemTimeZone].secondsFromGMT];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour
                                                                   fromDate:gmtDate];
    
    NSString *url = @"http://www.tenbyten.org/Data/global/";
    
    url = [url stringByAppendingPathComponent:[NSString stringWithFormat:@"%d", (int)components.year]];
    
    NSString *month = [NSString stringWithFormat:@"%d", (int)components.month];
    
    if(month.length < 2)
    {
        month = [NSString stringWithFormat:@"0%@", month];
    }
    
    url = [url stringByAppendingPathComponent:month];
    
    NSString *day = [NSString stringWithFormat:@"%d", (int)components.day];
    
    if(day.length < 2)
    {
        day = [NSString stringWithFormat:@"0%@", day];
    }
    
    url = [url stringByAppendingPathComponent:day];
    
    NSString *hour = [NSString stringWithFormat:@"%d", (int)components.hour];
    
    if(hour.length < 2)
    {
        hour = [NSString stringWithFormat:@"0%@", hour];
    }
    
    url = [url stringByAppendingPathComponent:hour];
    
    _url = [url stringByAppendingString:@"/"];
}

- (void)fetchAllImageObjectsComplition:(void (^)(void))complition
{
    NSURL *URL = [NSURL URLWithString:_url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (!connectionError)
                               {
                                   [self parseHtmlAndCreateImageObjects:data
                                                             complition:complition];
                               }
                               else
                               {
                                   if (complition)
                                   {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           
                                           complition();
                                       });
                                   }
                               }
                           }];
}

- (void)parseHtmlAndCreateImageObjects:(NSData *)data
                            complition:(void (^)(void))complition
{
    NSManagedObjectContext *localContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    localContext.parentContext = _context;
    
    [localContext performBlock:^{
        
        NSString *html = [[NSString alloc] initWithData:data
                                               encoding:NSUTF8StringEncoding];
        
        NSRange searchedRange = NSMakeRange(0, [html length]);
        NSString *pattern = @"<a href=\"(\\w{1,}.jpg)\">";
        
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                               options:0
                                                                                 error:nil];
        
        NSArray* matches = [regex matchesInString:html
                                          options:0
                                            range:searchedRange];
        
        for (NSTextCheckingResult* match in matches)
        {
            NSRange hrefRange = [match rangeAtIndex:1];
            NSString *href = [html substringWithRange:hrefRange];
            
            ImageObject *imageObject = [NSEntityDescription insertNewObjectForEntityForName:@"ImageObject"
                                                                     inManagedObjectContext:localContext];
            
            imageObject.url = [_url stringByAppendingString:href];
            imageObject.date = [NSDate date];
            
            if ([matches indexOfObject:match] % 10 == 9)
            {
                [localContext save:nil];
                
                usleep(50000);  //0.05 sec  //lol
            }
        }
        
        [localContext save:nil];
        
        if(complition)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                complition();
            });
        }
    }];
}

- (void)downloadFileByUrl:(NSString *)url
               complition:(void (^)(NSString *))complition
{
    if([_currentDownloadUrls objectForKey:url] == nil)
    {
        [_currentDownloadUrls setObject:@YES
                                 forKey:url];
        
        [_queue addOperationWithBlock:^{
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            
            NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            path = [path stringByAppendingPathComponent:[url lastPathComponent]];
            
            [imageData writeToFile:path
                        atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_currentDownloadUrls removeObjectForKey:url];
                
                if(complition)
                {
                    complition(path);
                }
            });
        }];
    }
}

- (void)cancelAll
{
    [_queue cancelAllOperations];
}

@end
