//
//  LoaderViewController.m
//  Mail Test
//
//  Created by Rinat Murtazin on 30.11.14.
//  Copyright (c) 2014 Rinat Murtazin. All rights reserved.
//

#import "LoaderViewController.h"
#import "DownloadManager.h"

@interface LoaderViewController ()

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicatorView;
@property (nonatomic, weak) IBOutlet UIView *containerView;


@end

@implementation LoaderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[DownloadManager manager] fetchAllImageObjectsComplition:^{
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             
                             _indicatorView.alpha = 0;
                             _containerView.alpha = 1;
                             
                         } completion:^(BOOL finished) {
                             
                             [_indicatorView removeFromSuperview];
                         }];
    }];
}

@end
